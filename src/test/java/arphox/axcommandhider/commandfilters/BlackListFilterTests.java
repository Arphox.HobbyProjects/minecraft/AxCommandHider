package arphox.axcommandhider.commandfilters;

import org.bukkit.permissions.Permissible;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class BlackListFilterTests {
    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "about",
            "ABOUT",
            "About",
            "abouT",
            "aBoUt",
            "anything",
            "help"
    })
    public void blocksNecessaryCommand_AndDoesNotCallNext(String command) {
        // Arrange
        ICommandFilter nextFilter = Mockito.mock(ICommandFilter.class);
        Collection<String> blacklist = Arrays.asList("help", "about", "oops", "anything", "cookies");
        ICommandFilter commandFilter = new BlackListFilter(blacklist, nextFilter);

        // Act
        boolean result = commandFilter.isAllowed(command, null);

        // Assert
        assertFalse(result);
        verify(nextFilter, times(0)).isAllowed(anyString(), any(Permissible.class));
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "halp",
            "hel",
            "elp",
            "helpme",
            "_about",
            "about_",
            "whatever"
    })
    public void doesNotBlocksCommandThatIsNotOnBlackList_AndReturnsWhatNextReturns(String command) {
        Collection<String> blacklist = Arrays.asList("help", "about", "anything");
        {
            ICommandFilter commandFilter = new BlackListFilter(blacklist, (c, p) -> true);
            boolean result = commandFilter.isAllowed(command, null);
            assertTrue(result);
        }
        {
            ICommandFilter commandFilter = new BlackListFilter(blacklist, (c, p) -> false);
            boolean result = commandFilter.isAllowed(command, null);
            assertFalse(result);
        }
    }
}