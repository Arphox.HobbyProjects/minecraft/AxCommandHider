package arphox.axcommandhider.commandfilters;

import org.bukkit.permissions.Permissible;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class RootCommandNameExtractorTests {
    private static Stream<Arguments> argumentsFor_callsNextWithModifiedValue() {
        return Stream.of(
                Arguments.of("hello", "hello"),
                Arguments.of("Hello", "Hello"),
                Arguments.of("hellO", "hellO"),
                Arguments.of("/help", "help"),
                Arguments.of("/help 1", "help"),
                Arguments.of("/about ", "about"),
                Arguments.of("/about P", "about"),
                Arguments.of("/about P/", "about"),
                Arguments.of("?", "?"),
                Arguments.of("/?", "?"),
                Arguments.of("//cut", "/cut"),
                Arguments.of(" /about", "about"),
                Arguments.of("  /twospace", "twospace"),
                Arguments.of("    /spaces", "spaces"),
                Arguments.of(" yolo", "yolo"),
                Arguments.of("  yolo", "yolo")
        );
    }

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @ParameterizedTest
    @ValueSource(booleans = {
            true,
            false
    })
    public void returnsWhatNextReturns(boolean whatNextShouldReturn) {
        // Arrange
        ICommandFilter nextFilter = Mockito.mock(ICommandFilter.class);
        Mockito.when(nextFilter.isAllowed(anyString(), any(Permissible.class))).thenReturn(whatNextShouldReturn);
        ICommandFilter sut = new RootCommandNameExtractor(nextFilter);

        // Act
        boolean result = sut.isAllowed("whatever", null);

        // Assert
        assertEquals(whatNextShouldReturn, result);
    }

    @ParameterizedTest
    @MethodSource("argumentsFor_callsNextWithModifiedValue")
    public void callsNextWithModifiedValue(String original, String modified) {
        // Arrange
        ICommandFilter nextFilter = Mockito.mock(ICommandFilter.class);
        ICommandFilter sut = new RootCommandNameExtractor(nextFilter);
        Permissible permissible = Mockito.mock(Permissible.class);

        // Act
        sut.isAllowed(original, permissible);

        // Assert
        verify(nextFilter, times(1)).isAllowed(modified, permissible);
    }
}