package arphox.axcommandhider.config;

/* Note:
    Insert the example .yml file into src/main/resources,
    because when the config is loaded first,
    the example file is used as configuration.
*/

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;

/**
 * A simple .yml file handler.
 *
 * @author Arphox
 */
public final class BukkitYAMLConfigFile {
    // based on: http://bukkit.gamepedia.com/Configuration_API_Reference

    private final JavaPlugin pluginInstance;
    private final String fileName;
    private FileConfiguration fileConfig = null;
    private File file;

    /**
     * Initializes a configuration file. Details: ceates a folder for the plugin
     * if needed, then loads the configuration if exists, or loads the config
     * from the src/main/resources folder and copies it to the plugin's folder.
     *
     * @param pluginInstance The runner plugin's instance
     * @param fileName       The file to handle, e.g. "someConfig.yml"
     * @throws IllegalArgumentException If any parameter is null.
     */
    public BukkitYAMLConfigFile(JavaPlugin pluginInstance, String fileName) throws IllegalArgumentException {
        if (pluginInstance == null) {
            throw new IllegalArgumentException("The plugin instance cannot be null.");
        }
        if (fileName == null) {
            throw new IllegalArgumentException("The file name cannot be null.");
        }
        if (!fileName.endsWith(".yml")) {
            fileName += fileName + ".yml";
        }

        this.pluginInstance = pluginInstance;
        this.fileName = fileName;
        this.file = new File(this.pluginInstance.getDataFolder(), this.fileName);
        createPluginFolderIfNeeded();
        if (file.exists()) {
            fileConfig = YamlConfiguration.loadConfiguration(file);
        } else {
            loadDefaults();
            pluginInstance.saveResource(this.fileName, false);
            fileConfig = YamlConfiguration.loadConfiguration(file);
        }
    }

    private void loadDefaults() {
        fileConfig = YamlConfiguration.loadConfiguration(file);

        // Look for defaults in the jar
        Reader defConfigStream = null;
        defConfigStream = new InputStreamReader(this.pluginInstance.getResource(this.fileName), StandardCharsets.UTF_8);

        if (defConfigStream != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            fileConfig.setDefaults(defConfig);
        }
    }

    public FileConfiguration getConfig() {
        return fileConfig;
    }

    public void saveConfig() {
        try {
            fileConfig.save(file);
        } catch (IOException ex) {
            Bukkit.getLogger().log(Level.SEVERE, "Could not save config to " + file, ex);
        }
    }

    public void reloadConfig() {
        fileConfig = YamlConfiguration.loadConfiguration(file);
    }

    private void createPluginFolderIfNeeded() {
        File dir = this.pluginInstance.getDataFolder();
        if (!dir.exists()) {
            dir.mkdirs();
        }
    }
}