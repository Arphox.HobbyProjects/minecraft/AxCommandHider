package arphox.axcommandhider;

public enum VersionType {
    Unsupported,
    V8,
    V10,
    V13
}