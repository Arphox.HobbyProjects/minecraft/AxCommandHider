package arphox.axcommandhider;

import arphox.axcommandhider.bstats.MetricsLite;
import arphox.axcommandhider.config.ConfigLoader;
import arphox.axcommandhider.config.Configuration;
import arphox.axcommandhider.versionlogics.IVersionLogic;
import arphox.axcommandhider.versionlogics.VersionLogicFactory;
import org.bukkit.plugin.java.JavaPlugin;

public class AxCommandHider extends JavaPlugin {
    private Configuration configuration;

    public Configuration getConfiguration() {
        return configuration;
    }

    @Override
    public void onEnable() {
        enableMetrics();
        configuration = new ConfigLoader().load(this);
        VersionType logicType = new ServerVersionFinder().determineServerVersion(this.getServer().getVersion());
        VersionLogicFactory logicFactory = new VersionLogicFactory(this);
        IVersionLogic logic = logicFactory.create(logicType);
        logic.enable();
    }

    private void enableMetrics() {
        new MetricsLite(this, 6942);
    }
}