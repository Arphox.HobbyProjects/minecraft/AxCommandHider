package arphox.axcommandhider;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ServerVersionFinder {
    public static final Pattern versionMatchingPattern = Pattern.compile("\\(MC: (.*)\\)");

    public VersionType determineServerVersion(String serverVersion) {
        int versionNumber = getCustomVersionNumber(serverVersion);

        if (versionNumber < 108)
            return VersionType.Unsupported;
        if (versionNumber < 110)
            return VersionType.V8;
        if (versionNumber < 113)
            return VersionType.V10;

        return VersionType.V13;
    }

    private int getCustomVersionNumber(String serverVersion) {
        String versionPart = getVersionPart(serverVersion);
        int[] versionNumbers = Arrays.stream(versionPart.split("\\.")).mapToInt(Integer::parseInt).toArray();
        int major = versionNumbers[0];
        int minor = versionNumbers[1];
        return major * 100 + minor;
    }

    private String getVersionPart(String serverVersion) {
        Matcher matcher = versionMatchingPattern.matcher(serverVersion);
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            throw new RuntimeException(String.format("Cannot parse version string '%s'", serverVersion));
        }
    }
}