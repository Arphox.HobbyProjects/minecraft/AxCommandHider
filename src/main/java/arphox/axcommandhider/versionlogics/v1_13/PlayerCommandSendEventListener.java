package arphox.axcommandhider.versionlogics.v1_13;

import arphox.axcommandhider.commandfilters.ICommandFilter;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandSendEvent;

import java.util.Collection;

public class PlayerCommandSendEventListener implements Listener {

    private final ICommandFilter commandFilter;

    public PlayerCommandSendEventListener(ICommandFilter commandFilter) {
        this.commandFilter = commandFilter;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerCommandSendEvent(PlayerCommandSendEvent event) {
        Collection<String> commands = event.getCommands();
        Player player = event.getPlayer();

        if (!commands.isEmpty()) { // removeIf throws exception if collection is empty
            commands.removeIf(s -> !commandFilter.isAllowed("/" + s, player));
        }
    }
}