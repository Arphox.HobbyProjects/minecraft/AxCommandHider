## Developer documentation for handling version 1.13

Starting from 1.13, the Spigot-API contains the 
[`PlayerCommandSendEvent`](https://helpch.at/docs/1.13/org/bukkit/event/player/PlayerCommandSendEvent.html),
which is called when the server sends the list of available commands to the client.
This happens when the client joins the server.  
This event makes it possible to inspect and modify the list of "root commands
" those will be sent to the player.

Also, from 1.13, the API contains the
[`Player#updateCommands()`](https://helpch.at/docs/1.13/org/bukkit/entity/Player.html#updateCommands--)
method which does _"Update the list of commands sent to the client."_.
This method basically recalculates a player's permissions and sends the commands again,
just like when the player joined the server.

By using these two additions, it is possible to hide specific commands.

## V13Logic
When enabled, does 2 things:
1. Registers the `PlayerCommandSendEvent` handling logic
2. Updates the available commands for all online players.

### updateCommands() for all online players
Calls `updateCommands()` for all online players on the server.  
This is necessary to apply configuration changes to already online players.

### `PlayerCommandSendEvent` handling logic
`String`s found in this list are _root commands_.  
This is best demonstrated with an example:
```
?
bukkit:?
bukkit:help
help
me
minecraft:tell
msg
w
```
So it contains all commands without their leading first `/` character.  
_(Note: WorldEdit uses commands with double slash like `//br`, they appear without their first slash so like `/br`.)_

Here the logic simply filters the root command list, removes all hidden commands.  
But all sent in commands are prefixed with a `'/'` to indicate the filter that it is a command 
and its first `/` shouldn't be removed (for e.g. `//br`).  