package arphox.axcommandhider.versionlogics;

import java.util.regex.Pattern;

public final class Common {
    private static final Pattern pattern_isCommand = Pattern.compile("^\\s*/.*");

    private Common() {
    }

    public static boolean isCommand(String message) {
        return pattern_isCommand.matcher(message).find();
    }
}
