package arphox.axcommandhider.versionlogics.v1_8;

import arphox.axcommandhider.commandfilters.ICommandFilter;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import org.bukkit.plugin.java.JavaPlugin;

import static arphox.axcommandhider.versionlogics.Common.isCommand;

public class TabCompletionRequestWatcher extends PacketAdapter {
    private final ICommandFilter commandFilter;

    public TabCompletionRequestWatcher(JavaPlugin plugin, ICommandFilter commandFilter) {
        super(plugin, ListenerPriority.NORMAL, PacketType.Play.Client.TAB_COMPLETE);
        this.commandFilter = commandFilter;
    }

    @Override
    public void onPacketReceiving(PacketEvent event) {
        if (event.getPacketType() != PacketType.Play.Client.TAB_COMPLETE) {
            return;
        }

        String message = getMessage(event.getPacket());
        if (!isCommand(message) || !message.contains(" ")) {
            return;
        }

        if (!commandFilter.isAllowed(message, event.getPlayer()))
            event.setCancelled(true);
    }

    private String getMessage(PacketContainer packet) {
        return (String) packet.getModifier().read(0);
    }
}
