package arphox.axcommandhider.versionlogics.v1_8;

import arphox.axcommandhider.commandfilters.ICommandFilter;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import org.bukkit.plugin.java.JavaPlugin;

public class TabCompletionBlocker {
    public void initialize(JavaPlugin plugin, ICommandFilter commandFilter) {
        ProtocolManager protocolManager = ProtocolLibrary.getProtocolManager();
        protocolManager.addPacketListener(new TabCompletionRequestWatcher(plugin, commandFilter));
        protocolManager.addPacketListener(new TabCompletionResponseWatcher(plugin, commandFilter));
    }
}