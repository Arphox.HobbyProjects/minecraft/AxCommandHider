package arphox.axcommandhider.versionlogics.v1_8;

import arphox.axcommandhider.commandfilters.ICommandFilter;
import arphox.axcommandhider.versionlogics.IVersionLogic;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class V8Logic implements IVersionLogic {
    private final JavaPlugin plugin;
    private final ICommandFilter commandFilter;
    private TabCompletionBlocker tabCompletionBlocker; // held to avoid earlier GC collection, dunno if it is needed, make sure

    public V8Logic(JavaPlugin plugin, ICommandFilter commandFilter) {
        this.plugin = plugin;
        this.commandFilter = commandFilter;
    }

    private static boolean isProtocolLibDisabled(PluginManager pluginManager) {
        return !pluginManager.isPluginEnabled("ProtocolLib");
    }

    @Override
    public void enable() {
        if (isProtocolLibDisabled(plugin.getServer().getPluginManager())) {
            plugin.getLogger().severe("ProtocolLib is not enabled but necessary for this plugin to work.");
            plugin.getServer().getPluginManager().disablePlugin(plugin);
            return;
        }
        // Do NOT move the ProtocolLib existence checking to the class below
        // because then the JVM throws a NoClassDefFound exception because it tries to compile the full class.
        tabCompletionBlocker = new TabCompletionBlocker();
        tabCompletionBlocker.initialize(plugin, commandFilter);
    }
}