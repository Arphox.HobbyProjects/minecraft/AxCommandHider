package arphox.axcommandhider.versionlogics;

import arphox.axcommandhider.AxCommandHider;
import arphox.axcommandhider.VersionType;
import arphox.axcommandhider.commandfilters.CommandFilterAssembler;
import arphox.axcommandhider.commandfilters.ICommandFilter;
import arphox.axcommandhider.config.Configuration;
import arphox.axcommandhider.versionlogics.v1_10.V10Logic;
import arphox.axcommandhider.versionlogics.v1_13.V13Logic;
import arphox.axcommandhider.versionlogics.v1_8.V8Logic;

public class VersionLogicFactory {
    private final AxCommandHider plugin;

    public VersionLogicFactory(AxCommandHider plugin) {
        this.plugin = plugin;
    }

    public IVersionLogic create(VersionType versionType) {
        switch (versionType) {
            case Unsupported:
                return new UnsupportedVersionLogic(plugin);
            case V8:
                return new V8Logic(plugin, createCommandFilter());
            case V10:
                return new V10Logic(plugin, createCommandFilter());
            case V13:
                return new V13Logic(plugin, createCommandFilter());
        }

        throw new RuntimeException(String.format("Unknown enum member %s, please report this bug to the developer!", versionType));
    }

    private ICommandFilter createCommandFilter() {
        Configuration config = plugin.getConfiguration();
        return new CommandFilterAssembler().Assemble(config);
    }
}