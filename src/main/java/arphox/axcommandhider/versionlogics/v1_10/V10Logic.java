package arphox.axcommandhider.versionlogics.v1_10;

import arphox.axcommandhider.commandfilters.ICommandFilter;
import arphox.axcommandhider.versionlogics.IVersionLogic;
import org.bukkit.plugin.java.JavaPlugin;

public class V10Logic implements IVersionLogic {
    private final JavaPlugin plugin;
    private final ICommandFilter commandFilter;

    public V10Logic(JavaPlugin plugin, ICommandFilter commandFilter) {
        this.plugin = plugin;
        this.commandFilter = commandFilter;
    }

    @Override
    public void enable() {
        TabCompleteEventListener listener = new TabCompleteEventListener(commandFilter);
        plugin.getServer().getPluginManager().registerEvents(listener, plugin);
    }
}