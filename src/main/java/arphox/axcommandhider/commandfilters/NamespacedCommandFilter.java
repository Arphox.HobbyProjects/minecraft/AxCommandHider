package arphox.axcommandhider.commandfilters;

import org.bukkit.permissions.Permissible;

public class NamespacedCommandFilter implements ICommandFilter {
    private final boolean isEnabled;
    private final ICommandFilter nextFilter;

    public NamespacedCommandFilter(boolean isEnabled, ICommandFilter nextFilter) {
        this.isEnabled = isEnabled;
        this.nextFilter = nextFilter;
    }

    @Override
    public boolean isAllowed(String command, Permissible permissible) {
        if (isEnabled && command.contains(":"))
            return false;
        else
            return nextFilter.isAllowed(command, permissible);
    }
}