package arphox.axcommandhider.commandfilters;

import org.bukkit.permissions.Permissible;

import java.util.regex.Pattern;

public class RootCommandNameExtractor implements ICommandFilter {
    private static final Pattern pattern_removeLeadingWhitespaces = Pattern.compile("^\\s+");
    private final ICommandFilter nextFilter;

    public RootCommandNameExtractor(ICommandFilter nextFilter) {
        this.nextFilter = nextFilter;
    }

    @Override
    public boolean isAllowed(String command, Permissible permissible) {
        String rootCommand = getRootCommand(command);
        return nextFilter.isAllowed(rootCommand, permissible);
    }

    private String getRootCommand(String str) {
        str = removeLeadingWhitespaces(str);
        str = removeLeadingSlash(str);
        str = removeSubCommandParts(str);
        return str;
    }

    private String removeLeadingWhitespaces(String str) {
        return pattern_removeLeadingWhitespaces.matcher(str).replaceFirst("");
    }

    private String removeLeadingSlash(String str) {
        if (str.startsWith("/"))
            return str.substring(1); // remove leading '/'
        else
            return str;
    }

    private String removeSubCommandParts(String str) {
        if (str.contains(" "))
            return str.split(" ")[0];
        else
            return str;
    }
}