package arphox.axcommandhider.commandfilters;

import org.bukkit.permissions.Permissible;

public interface ICommandFilter {
    boolean isAllowed(String command, Permissible permissible);
}
