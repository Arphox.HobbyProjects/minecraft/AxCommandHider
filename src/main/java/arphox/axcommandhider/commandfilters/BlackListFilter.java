package arphox.axcommandhider.commandfilters;

import org.bukkit.permissions.Permissible;

import java.util.Collection;

public final class BlackListFilter implements ICommandFilter {
    private final Collection<String> commandBlackList;
    private final ICommandFilter nextFilter;

    public BlackListFilter(Collection<String> commandBlackList, ICommandFilter nextFilter) {
        this.commandBlackList = commandBlackList;
        this.nextFilter = nextFilter;
    }

    @Override
    public boolean isAllowed(String command, Permissible permissible) {
        boolean isBlocked = commandBlackList.stream().anyMatch(command::equalsIgnoreCase);
        if (isBlocked)
            return false;
        else
            return nextFilter.isAllowed(command, permissible);
    }
}